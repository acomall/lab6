/*******************
 Bucky O'Malley
 acomall
 Lab 5
 Section 004
 Hunter Ross
*******************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"

using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
  //counter variables
  int i = 0;
  int j = 0;
  //array to hold 52 cards of type Card from the struct
  Card cardDeck[52];
  //counter variable
  int count = 0;

  //Assigns a suit and value to each card in the deck
  for (i = 0; i < 4; i++)
  {
    for (j = 2; j < 15; j++)
    {
      cardDeck[count].suit = static_cast<Suit>(i);
      cardDeck[count].value = j;
      count++;
    }
  }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   random_shuffle(cardDeck, cardDeck + 52, myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    Card handCards[5];        //array to hold the cards (type card) in hand

    for (i = 0; i < 5; i++)       //deals out 5 cards
    {
      handCards[i] = cardDeck[i];
    }


    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
    /*sorts the hand sending it the first and last card in the hand and
    suit_order funciton*/
    sort(handCards, handCards + 5, suit_order);


    //TODO:
    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */

     //will print 5 times for the 5 cards in a hand
     for (i = 0; i < 5; i++)
     {
       cout << setw(10) << right << get_card_name(handCards[i]) << " of ";
       cout << get_suit_code(handCards[i]) << endl;
     }



  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
  //if left most card is less than right card...
  if (lhs.suit < rhs.suit)
  {
    //return true
    return true;
  }
  //if right most card is less than left most card
  //if right most card's suit is the same as the left most card's
  else if (rhs.suit == lhs.suit)
  {
    //determine which has a higher value
    if (lhs.value > rhs.value)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENT
  //integer to hold the card "number"
  int cardVal;
  //assigns the card number to cardVal
  cardVal = c.value;


  //if the card number is greater than 10 (a suit)...
  if (cardVal == 11)
  {
    //return "Jack"
    return "Jack";
  }
  //if it is 12
  else if (cardVal == 12)
  {
    //return "Queen"
    return "Queen";
  }
  //if it is 13
  else if(cardVal == 13)
  {
    //return "King"
    return "King";
  }
  //if it is 14
  else if (cardVal == 14)
  {
    //return "Ace"
    return "Ace";
  }
  else
  {
    return to_string(cardVal);
  }

}
